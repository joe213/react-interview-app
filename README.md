# react-interview-app

Repository hosting the toy app used for practical SR.ai React interviews. It was created with the `create-react-app` npx instruction.

Prior to the interview you should clone this repository, install dependencies, and verify that the app will run locally on your machine.

## Task

This repository tasks you with creating a very simple todo app. The app has the following business requirements:

* Users must be able to create todo items. The items must *at least* have a title.
* Created items are rendered in a todo list. The todo list is a list of todo items.
* Users must be able to mark todo items as completed. When the item is marked completed it should no longer appear in the todo list.
* The todo list should be accessed at route `/todo`. For example, if your dev server is running locally on port 3000, the full URL would be `http://localhost:3000/todo`.

It also has the following technical requirements:

* Components must be *functions*, not classes (see the distinction [here](https://reactjs.org/docs/components-and-props.html#function-and-class-components) in case you've been out of React world for awhile). These components may (and in some cases should) be stateful.
* Any "APIs" must be implemented as mocks.
* There are no styling requirements, but it would be nice to make the app at least a bit pleasing to look at :)

There is some very lightweight scafolding set up to get you started:

* Empty component definitions can be found at `src/pages/Todo/index.js`
* Unimplemented "API" functions can be found at `src/services/todo.js`. I use quotes because they are mocks. There is no functional backend; todo items are stored in memory in this codebase.
* `react-router-dom` (https://reactrouter.com/web/guides/quick-start) is installed, should you want to use it for routing.
* `mui` is installed should you want to use it for styled components.

Have a look through the comments in the code if you're looking for some inspiration.

**You are free to change the codebase however you please.** The code that's there now is just a hint. It is, however, a **requirement** that API mocks are used in some fashion (i.e. there needs to be some separation of the "APIs" from the components).

**You are not expected to complete the whole project in the 1 hour window.** We're more interested in how you think and work than we are in your time to completion.

You are free to work on the implementation ahead of time, but beware that we'll ask you to start from scratch for the actual interview. This gives us a chance to interact with you as you implement the app.

Good luck!
