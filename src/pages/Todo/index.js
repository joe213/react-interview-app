import 'react';

import { createTodo, completeTodo, listTodos } from 'services/todo';

// functional component that, given some props,
// must render a todo item.
// the component has the following requirements:
// 1) it must take the props required to render an
//    actual todo item (i.e. the title of the item)
// 2) it must have some interactive element (i.e. a
//    button or checkbox or some such) that, when clicked,
//    must mark the item as completed and remove it
//    from the todo list by calling the `createTodo` "API"
// 3) it must render the actual item and the interactive
//    element used to mark it as completed.
function TodoItem(props) {
  return;
}

// functional component that renders all of the
// user's *outstanding* todo items.
// the component has the following requirements:
// 1) it should render a list of TodoItem (the
//    above component). how you implement this is
//    up to you.
function TodoList(props) {
  return;
}

// functional component that acts as a container
// of TodoList and/or TodoItems, depending on your
// implementation approach.
// the component has the following requirements:
// 1) "fetches" todo items from the `listTodos` "API"
// 2) renders the TodoList
// 3) handles creating a new todo through the `createTodo`
//    "API". that new todo item should of course be
//    included in the TodoList.
function TodoApp(props) {
  return;
}

