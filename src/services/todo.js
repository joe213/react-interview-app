// holds todo items. you can change this
// to another data structure if you like.
const TODOS = [];

// function simulating an API call to create
// a todo item. the created todo item should
// be "persisted" to the above TODOS const
async function createTodo() {
  throw 'Not implemented.';
}

// function simulating an API call to mark
// a todo item as completed. this should e.g.
// pop the todo from the above TODOS const
async function completeTodo() {
  throw 'Not Implemented.';
}

// function that lists todos. it should return
// items from the TODOS const *ordered by item
// creation time descending* (more recently
// created items are higher in the ordering)
async function listTodos() {
  throw 'Not Implemented.';
}

export { createTodo, completeTodo, listTodos }
