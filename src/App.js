import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function WelcomeComponent() {
  return (
    <p>
      Welcome. Follow the README to get started.
    </p>
  );
}

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={WelcomeComponent} />
      </Switch>
    </Router>
  );
}

export default App;
